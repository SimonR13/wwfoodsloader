package com.mycompany.wwfoodsloader;

import com.mycompany.wwfoodsloader.beans.FoodBean;
import com.mycompany.wwfoodsloader.beans.QueryHitsBean;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLController implements Initializable {
    private final DirectoryChooser dirChooser = new DirectoryChooser();
    private final ObservableList<FoodBean> foodListObserv = FXCollections.observableArrayList();
    private final ObservableList<QueryHitsBean> queryListObserv = FXCollections.observableArrayList();
    
    private Stage listStage = new Stage();
    private Stage fileStage = new Stage();
    private File selectedFolder = null;
    
    @FXML
    public Label label;
    
    @FXML
    public void handleButtonAction(ActionEvent event) {
        showFileSelection();
        /*(ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (observable.getValue().length() > 0) {
                final File respFile = ResponseParser.getInstance().openFile(selectedFolder.getPath() + File.separator + observable.getValue());
                final List<FoodBean> foodList = ResponseParser.getInstance().parseFileForWwFoodsInComposed(respFile);
                if (foodList != null && !foodList.isEmpty()) {
                    foodListObserv.addAll(foodList);
                    displayFoods();
                }
                else {
                    final List<QueryHitsBean> queryList = ResponseParser.getInstance().parseFileForFoodInQuery(respFile);
                    if (queryList != null && !queryList.isEmpty()) {
                        queryListObserv.addAll(queryList);
                        displayQueryResults();
                    }
                }
            }
        });*/
    }
        
    @FXML
    public void handleQueryDb(ActionEvent event) {
        VistaNavigator.loadVista(VistaNavigator.VISTA_QUERY_DB);
    }

    @FXML
    public void handlePrint(ActionEvent event) {
        VistaNavigator.loadVista(VistaNavigator.VISTA_PRINT);
    }
    
    private void showFileSelection() {
        final Group rootGrp = new Group();
        final Scene scene = createScene(rootGrp, 800, 600);
        rootGrp.setAutoSizeChildren(true);
        rootGrp.minHeight(700);
        rootGrp.minWidth(850);
        resetFileStage(scene);
        
        final ObservableList<String> filesInFolder = FXCollections.observableArrayList();
        if (selectedFolder != null && selectedFolder.isDirectory() && selectedFolder.canRead()) {
            filesInFolder.addAll(selectedFolder.list());
        }
        final ListView<String> selectedList = new ListView<>();
        final ListView<String> listView = new ListView<>();
        listView.setItems(filesInFolder);
        listView.setMinWidth(200);
        listView.setMaxWidth(200);
        
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            selectedList.setItems(listView.getSelectionModel().getSelectedItems());
        });
        
        final Button mergeButton = new Button("Merge Files");
        mergeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            final File mergedFiles = new File("/home/simon/Downloads/2_ww/merged.json");
            try {
                mergedFiles.createNewFile();
            } 
            catch (IOException ex) {
                Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            
            final List<QueryHitsBean> queryHitsBeanList = new ArrayList<>();
            final List<FoodBean> mergeList = new ArrayList<>();
            if (mergedFiles.exists() && mergedFiles.canWrite()) {
                for (File srcFile : selectedFolder.listFiles()) {
                    try {
                        List<?> tmpList = ResponseParser.getInstance().parseFileForWwFoodsInComposed(srcFile);
                        if (tmpList != null && !(tmpList.isEmpty())) {
                            mergeList.addAll((Collection<? extends FoodBean>) tmpList);
                        }
                        else {
                            tmpList = ResponseParser.getInstance().parseFileForFoodInQuery(srcFile);
                            for (Object obj : tmpList) {
                                final QueryHitsBean qHitsBean = (QueryHitsBean) obj;
                                final FoodBean newBean = new FoodBean();
                                newBean.setId(qHitsBean.getId());
                                newBean.setDisplayName(qHitsBean.getDisplayName());
                                newBean.setServingDesc(qHitsBean.getServingDesc());
                                newBean.setIngredientName(qHitsBean.getIngredientName());
                                newBean.setPortionId(qHitsBean.getPortionId());
                                newBean.setSourceType(qHitsBean.getSourceType());
                                newBean.setType(qHitsBean.getType());
                                newBean.setSmartPoints(qHitsBean.getSmartPoints());
                                newBean.setVersionId(qHitsBean.getVersionId());
                                newBean.setPoints(qHitsBean.getPoints());
                                newBean.setQty(qHitsBean.getQty());
                                newBean.setIsPowerFood(qHitsBean.getIsPowerFood());
                                newBean.setObjectID(qHitsBean.getObjectID());
                                mergeList.add(newBean);
                            }
                        }
                    }
                    catch (Exception e) {
                        Logger.getLogger(ResponseParser.class.getName()).log(Level.SEVERE, null, e);
                        continue;
                    }
                }
                ResponseParser.getInstance().writeFoodBeansToFile(mergedFiles, mergeList);
            }
            
            display2Print(mergeList);
        });
        
        final Button showButton = new Button("Anzeigen");
        showButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            foodListObserv.clear();
            queryListObserv.clear();
            selectedList.getItems()
                .stream()
                .map((fileName) -> ResponseParser.getInstance().openFile(selectedFolder.getPath() + File.separator + fileName))
                .forEach((respFile) -> {
                    List<?> tmpList = ResponseParser.getInstance().parseFileForWwFoodsInComposed(respFile);
                    if (tmpList != null && !(tmpList.isEmpty())) {
                        foodListObserv.addAll((Collection<? extends FoodBean>) tmpList);
                    }
                    else {
                        tmpList = ResponseParser.getInstance().parseFileForFoodInQuery(respFile);
                        if (tmpList != null && !tmpList.isEmpty()) {
                            queryListObserv.addAll((Collection<? extends QueryHitsBean>) tmpList);
                        }
                    }
                });
            
            TableView<?> tableViewTmp = (TableView<?>) scene.lookup("#tableView");
            if (!(foodListObserv.isEmpty())) {
                setupFoodBeanTableView((TableView<FoodBean>) tableViewTmp);
                tableViewTmp.getItems().clear();
                ((TableView<FoodBean>) tableViewTmp).getItems().addAll(foodListObserv);
            }
            else if (!(queryListObserv.isEmpty())) {
                setupQueryResultTableView((TableView<QueryHitsBean>) tableViewTmp);
                tableViewTmp.getItems().clear();
                ((TableView<QueryHitsBean>) tableViewTmp).getItems().addAll(queryListObserv);
            }

            tableViewTmp.refresh();
        });
        
        final TableView<?> tableView = new TableView<>();
        tableView.setId("tableView");
        
        final SplitPane splitPane = new SplitPane();
//        splitPane.setMinSize(700, 500);
        splitPane.setDividerPositions(0.4F);
        
        final StackPane stackPane1List = new StackPane();
        stackPane1List.getChildren().add(listView);
        
        final StackPane stackPaneTable = new StackPane();
        stackPaneTable.getChildren().add(tableView);
        
        splitPane.getItems().addAll(stackPane1List, stackPaneTable);
        
        // USE A LAYOUT VBOX FOR EASIER POSITIONING OF THE VISUAL NODES ON SCENE
        final HBox hBox = new HBox(10);
        hBox.setAlignment(Pos.TOP_CENTER);
        hBox.getChildren().add(showButton);
        hBox.getChildren().add(mergeButton);
        
        final VBox vBox = new VBox(10, hBox, (Node) splitPane);
//        vBox.setPadding(new Insets(60, 0, 0, 20));
        vBox.setAlignment(Pos.TOP_CENTER);

        //add all nodes to main root group
        rootGrp.getChildren().add(vBox);
        //show the stage
        fileStage.show();
    }
    
    private TableView<QueryHitsBean> createQueryResultsTableView() {
        final TableView<QueryHitsBean> newTable = new TableView<>(queryListObserv);
        
        setupQueryResultTableView(newTable);
        
        return newTable;
    }

    private void setupQueryResultTableView(final TableView<QueryHitsBean> newTable) {
        TableColumn idCol = new TableColumn<>("Food Id");
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn anzahlCol = new TableColumn<>("Anzahl");
        TableColumn portionCol = new TableColumn<>("Portion");
                
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        idCol.setMinWidth(50);
        idCol.setResizable(true);
        nameCol.setCellValueFactory(new PropertyValueFactory<>("displayName"));
        nameCol.setMinWidth(250);
        nameCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        anzahlCol.setCellValueFactory(new PropertyValueFactory("qty"));
        anzahlCol.setMinWidth(50);
        anzahlCol.setMaxWidth(50);
        portionCol.setCellValueFactory(new PropertyValueFactory<>("servingDesc"));
        portionCol.setMinWidth(50);
        portionCol.setResizable(true);
        
        newTable.getColumns().addAll(idCol, nameCol, spCol, anzahlCol, portionCol);
        newTable.setMinSize(850, 700);
    }
    
    private TableView<FoodBean> createFoodBeanTableView() {
        final TableView<FoodBean> newTable = new TableView<>(foodListObserv);
        
        setupFoodBeanTableView(newTable);
        
        return newTable;
    }

    private void setupFoodBeanTableView(final TableView<FoodBean> newTable) {
        TableColumn idCol = new TableColumn<>("Food Id");
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn foodTypeCol = new TableColumn<>("Quelle");
        TableColumn portionCol = new TableColumn<>("Portion");
        TableColumn pAmountCol = new TableColumn<>("Menge");
        TableColumn pUnitCol = new TableColumn<>("Einheit");
                
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameCol.setMinWidth(100);
        nameCol.setResizable(true);
        idCol.setCellValueFactory(new PropertyValueFactory<>("entryId"));
        idCol.setMinWidth(100);
        idCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        foodTypeCol.setCellValueFactory(new PropertyValueFactory<>("sourceType"));
        foodTypeCol.setMinWidth(100);
        foodTypeCol.setResizable(true);
        portionCol.setCellValueFactory(new PropertyValueFactory("servingDesc"));
        portionCol.setResizable(true);
        pAmountCol.setCellValueFactory(new PropertyValueFactory("portionSize"));
        pAmountCol.setResizable(true);
        pUnitCol.setCellValueFactory(new PropertyValueFactory("portionName"));
        pUnitCol.setResizable(true);
        
        newTable.getColumns().addAll(idCol, nameCol, spCol, foodTypeCol, portionCol, pAmountCol, pUnitCol);
        newTable.setMinSize(850, 700);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        dirChooser.setInitialDirectory();
//        dirChooser.setTitle("Select Folder!");
        selectedFolder = new File("/home/simon/Downloads/2_ww/"); //dirChooser.showDialog(new Stage());
    }    
    
    private Scene createScene(final Group rootGrp, double widthPx, double heightPx) {
        final Scene scene = new Scene(rootGrp, widthPx, heightPx, Color.TRANSPARENT);
        return scene;
    }
    
    private void resetFileStage(final Scene sce) {
        fileStage.close();
        fileStage = new Stage(StageStyle.DECORATED);
        fileStage.setScene(sce);
        fileStage.centerOnScreen();
    }
    
    private void newScene(Object obj) {
        final Group rootGrp = new Group();
        final Scene scene = createScene(rootGrp, 300, 600);
        resetFileStage(scene);
        //show the stage
        fileStage.show();

        // USE A LAYOUT VBOX FOR EASIER POSITIONING OF THE VISUAL NODES ON SCENE
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(60, 0, 0, 20));
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().add((Node) obj);

        //add all nodes to main root group
        rootGrp.getChildren().add(vBox);
    }
    private Scene newScene(Object obj, Double width, Double height) {
        listStage.close();
        //create stage which has set stage style transparent
        listStage = new Stage(StageStyle.DECORATED);
        //create root node of scene, i.e. group
        Group rootGroup = new Group();
        //create scene with set width, height and color
        Scene scene = new Scene(rootGroup, width, height, Color.TRANSPARENT);
        //set scene to stage
        listStage.setScene(scene);
        //center stage on screen
        listStage.centerOnScreen();
        //show the stage
        listStage.show();

        // USE A LAYOUT VBOX FOR EASIER POSITIONING OF THE VISUAL NODES ON SCENE
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(60, 0, 0, 20));
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().add((Node) obj);
        vBox.setId("vBox");

        //add all nodes to main root group
        rootGroup.getChildren().add(vBox);
        
        return scene;
    }

    private void displayTableView(TableView<?> tableView) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void display2Print(List<FoodBean> foodBeanList) {
        final TableView newFoodTable = createFoodBeanTableView();
        foodListObserv.clear();
        foodListObserv.addAll(foodBeanList);
        newFoodTable.setItems(foodListObserv);
        newFoodTable.setMinSize(700D, 500D);
        newFoodTable.setMaxSize(TableView.USE_COMPUTED_SIZE, TableView.USE_COMPUTED_SIZE);
        
        final Scene beanScene = newScene(newFoodTable, 700D, 500D);
        final VBox vbox = (VBox) beanScene.lookup("#vBox");
        final Button printButton = new Button("Print");
        printButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> { 
            printTableView(newFoodTable);
        });
        vbox.getChildren().add(printButton);
    }
    
    private void printTableView(final TableView<?> tableView) {
        final PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null) {
            PageLayout pl = 
                    job.getPrinter().createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.EQUAL);
            
            boolean success = job.printPage(pl, tableView);
            if (success) {
               job.endJob();
            }
        }
    }
}
