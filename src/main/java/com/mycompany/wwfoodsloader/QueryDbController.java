/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader;

import com.mycompany.wwfoodsloader.beans.FoodBean;
import com.mycompany.wwfoodsloader.beans.QueryHitsBean;
import com.mycompany.wwfoodsloader.connection.HttpConnectionProvider;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author simon
 */
public class QueryDbController implements Initializable {
    final HttpConnectionProvider httpProvider = HttpConnectionProvider.getInstance();
    final ObservableList<FoodBean> foodListObserv = FXCollections.observableArrayList();
    final ObservableList<QueryHitsBean> queryListObserv = FXCollections.observableArrayList();
            
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Pane simplePane;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private TextField queryString;

    @FXML
    private void handleOpenConnection(ActionEvent event) {
        // TODO
    }
    
    @FXML
    private void handleRecent(ActionEvent event) {
        new Thread() {
            @Override
            public void run() {
                final List<FoodBean> recentFoodsList = httpProvider.getRecentFoods();
                foodListObserv.clear();
                foodListObserv.addAll(recentFoodsList);
            }
        }.start();
    }
    
    @FXML
    private void handleFavorites(ActionEvent event) {
        new Thread() {
            @Override
            public void run() {
                final List<FoodBean> favoriteFoodList = httpProvider.getFavoriteFoods();
                foodListObserv.clear();
                foodListObserv.addAll(favoriteFoodList);
            }
        }.start();
    }
    
    @FXML
    private void handleSearch(ActionEvent event) {
        new Thread() {
            @Override
            public void run() {
                final List<QueryHitsBean> queryResultList = httpProvider.queryFoods(queryString.getText());
                queryListObserv.clear();
                queryListObserv.addAll(queryResultList);
            }
        }.start();
    }
    
    @FXML
    private void handleSearchFav(ActionEvent event) {
        new Thread() {
            @Override
            public void run() {
                final List<QueryHitsBean> queryResultList = httpProvider.queryFavoriteFoods(queryString.getText());
                queryListObserv.clear();
                queryListObserv.addAll(queryResultList);
            }
        }.start();
    }
    
    @FXML
    private void handleBack(ActionEvent event) {
        VistaNavigator.loadVista(VistaNavigator.VISTA_1);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (progressBar == null) {
            progressBar = new ProgressBar();
        }
        httpProvider.progressProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                progressBar.setProgress(newValue.doubleValue());
            }
        });
        if (foodListObserv != null) {
            foodListObserv.addListener(new ListChangeListener<FoodBean>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends FoodBean> c) {
                    while (c.next()) {
                        if (c.wasAdded()) {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    displayFoodBeans();
                                }
                            });
                            break;
                        }
                    }
                }
            });
        }
        if (queryListObserv != null) {
            queryListObserv.addListener(new ListChangeListener<QueryHitsBean>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends QueryHitsBean> c) {
                    while (c.next()) {
                        if (c.wasAdded()) {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    displayQueryResults();
                                }
                            });
                            break;
                        }
                    }
                }
            });
        }
    }
    
    private void displayFoodBeans() {
        final TableView<FoodBean> newTable = new TableView<>(foodListObserv);
        
        TableColumn idCol = new TableColumn<>("Food Id");
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn foodTypeCol = new TableColumn<>("Quelle");
        TableColumn portionCol = new TableColumn<>("Portion");
        TableColumn pAmountCol = new TableColumn<>("Menge");
        TableColumn pUnitCol = new TableColumn<>("Einheit");
                
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameCol.setMinWidth(100);
        nameCol.setResizable(true);
        idCol.setCellValueFactory(new PropertyValueFactory<>("entryId"));
        idCol.setMinWidth(100);
        idCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        foodTypeCol.setCellValueFactory(new PropertyValueFactory<>("sourceType"));
        foodTypeCol.setMinWidth(100);
        foodTypeCol.setResizable(true);
        portionCol.setCellValueFactory(new PropertyValueFactory("servingDesc"));
        portionCol.setResizable(true);
        pAmountCol.setCellValueFactory(new PropertyValueFactory("portionSize"));
        pAmountCol.setResizable(true);
        pUnitCol.setCellValueFactory(new PropertyValueFactory("portionName"));
        pUnitCol.setResizable(true);
        
        newTable.getColumns().addAll(idCol, nameCol, spCol, foodTypeCol, portionCol, pAmountCol, pUnitCol);
        simplePane.getChildren().clear();
        simplePane.getChildren().add(newTable);
    }
    
    private void displayQueryResults() {
        final TableView<QueryHitsBean> newTable = new TableView<>(queryListObserv);
        
        TableColumn idCol = new TableColumn<>("Food Id");
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn anzahlCol = new TableColumn<>("Anzahl");
        TableColumn portionCol = new TableColumn<>("Portion");
                
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        idCol.setMinWidth(50);
        idCol.setResizable(true);
        nameCol.setCellValueFactory(new PropertyValueFactory<>("displayName"));
        nameCol.setMinWidth(250);
        nameCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        anzahlCol.setCellValueFactory(new PropertyValueFactory("qty"));
        anzahlCol.setMinWidth(50);
        anzahlCol.setMaxWidth(50);
        portionCol.setCellValueFactory(new PropertyValueFactory<>("servingDesc"));
        portionCol.setMinWidth(50);
        portionCol.setResizable(true);
        
        newTable.getColumns().addAll(idCol, nameCol, spCol, anzahlCol, portionCol);
        newTable.setMinSize(850, 700);
        simplePane.getChildren().clear();
        simplePane.getChildren().add(newTable);
    }
}
