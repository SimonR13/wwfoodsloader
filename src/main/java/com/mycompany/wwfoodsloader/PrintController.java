/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader;

import com.mycompany.wwfoodsloader.beans.FoodBean;
import com.mycompany.wwfoodsloader.beans.QueryHitsBean;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author simon
 */
public class PrintController implements Initializable {
//    private final DirectoryChooser dirChooser = new DirectoryChooser();
    private final ObservableList<FoodBean> foodListObserv = FXCollections.observableArrayList();
    private final ObservableList<QueryHitsBean> queryListObserv = FXCollections.observableArrayList();
    
    private File selectedFolder = null;
    
    @FXML
    private Pane simplePane;
        
    @FXML
    public void handlePrintAllFiles(ActionEvent event) {
        final ObservableList<String> filesInFolderList = FXCollections.observableArrayList();
        if (selectedFolder != null && selectedFolder.isDirectory() && selectedFolder.canRead()) {
            filesInFolderList.addAll(selectedFolder.list());
        }
        
        for (String filePath : filesInFolderList) {
            File currFile = new File(filePath);
            final File respFile = ResponseParser.getInstance().openFile(selectedFolder.getPath() + File.separator + currFile);
            final List<FoodBean> foodList = ResponseParser.getInstance().parseFileForWwFoodsInComposed(respFile);
            if (foodList != null && !foodList.isEmpty()) {
                foodListObserv.addAll(foodList);
            }
            else {
                final List<QueryHitsBean> queryList = ResponseParser.getInstance().parseFileForFoodInQuery(respFile);
                if (queryList != null && !queryList.isEmpty()) {
                    queryListObserv.addAll(queryList);
                }
            }
            if (!foodListObserv.isEmpty()) {
                printFoodList();
            }
            if (!queryListObserv.isEmpty()) {
                printQueryResults();
            }
        }
    }
    
    @FXML
    public void handlePrintSelected(ActionEvent event) {
        showFileSelection(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (observable.getValue().length() > 0) {
                    final File respFile = ResponseParser.getInstance().openFile(selectedFolder.getPath() + File.separator + observable.getValue());
                    final List<FoodBean> foodList = ResponseParser.getInstance().parseFileForWwFoodsInComposed(respFile);
                    if (foodList != null && !foodList.isEmpty()) {
                        foodListObserv.addAll(foodList);
                        printFoodList();
                    }
                    else {
                        final List<QueryHitsBean> queryList = ResponseParser.getInstance().parseFileForFoodInQuery(respFile);
                        if (queryList != null && !queryList.isEmpty()) {
                            queryListObserv.addAll(queryList);
                            printQueryResults();
                        }
                    }
                }
            }
        });
    }
    
    private void printFoodList() {
        final TableView<FoodBean> tableView = getFoodBeanTableView();
        printTableView(tableView);
    }
    
    private void printTableView(final TableView<?> tableView) {
        final PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null) {
            PageLayout pl = 
                    job.getPrinter().createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.EQUAL);
            
            boolean success = job.printPage(pl, tableView);
            if (success) {
               job.endJob();
            }
        }
    }
    
    private void printQueryResults() {
        final TableView<QueryHitsBean> tableView = getQueryResultsTableView();
        printTableView(tableView);
    }
    
    private void showFileSelection(final ChangeListener<String> changeListener) {
        final ObservableList<String> filesInFolderList = FXCollections.observableArrayList();
        if (selectedFolder != null && selectedFolder.isDirectory() && selectedFolder.canRead()) {
            filesInFolderList.addAll(selectedFolder.list());
        }
        
        final ListView filesInFolder = new ListView();
        filesInFolder.setMinSize(simplePane.getWidth(), simplePane.getHeight());
        filesInFolder.getItems().clear();
        filesInFolder.setItems(filesInFolderList);
        
        filesInFolder.getFocusModel().focusedItemProperty().addListener(changeListener);

        simplePane.getChildren().clear();
        simplePane.getChildren().add(filesInFolder);
        
//        newScene(filesInFolder);
    }
    
    @FXML
    private void handleBack(ActionEvent event) {
        VistaNavigator.loadVista(VistaNavigator.VISTA_1);
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        selectedFolder = new File("/home/simon/Downloads/2_ww/");
    } 
    
    private TableView<QueryHitsBean> getQueryResultsTableView() {
        final TableView<QueryHitsBean> newTable = new TableView<>(queryListObserv);
        
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn anzahlCol = new TableColumn<>("Anzahl");
        TableColumn portionCol = new TableColumn<>("Portion");
                
        nameCol.setCellValueFactory(new PropertyValueFactory<>("displayName"));
        nameCol.setMinWidth(250);
        nameCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        anzahlCol.setCellValueFactory(new PropertyValueFactory("qty"));
        anzahlCol.setMinWidth(50);
        anzahlCol.setMaxWidth(50);
        portionCol.setCellValueFactory(new PropertyValueFactory<>("servingDesc"));
        portionCol.setMinWidth(50);
        portionCol.setResizable(true);
        
        newTable.getColumns().addAll(nameCol, spCol, anzahlCol, portionCol);
        newTable.setMinSize(850, 700);
        
        return newTable;
    }
    
    private TableView<FoodBean> getFoodBeanTableView() {
        final TableView<FoodBean> newTable = new TableView<>(foodListObserv);
        
        TableColumn nameCol = new TableColumn<>("Food Name");
        TableColumn spCol = new TableColumn<>("SmartPoints");
        TableColumn foodTypeCol = new TableColumn<>("Quelle");
        TableColumn portionCol = new TableColumn<>("Portion");
        TableColumn pAmountCol = new TableColumn<>("Menge");
        TableColumn pUnitCol = new TableColumn<>("Einheit");
                
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameCol.setMinWidth(100);
        nameCol.setResizable(true);
        spCol.setCellValueFactory(new PropertyValueFactory<>("smartPoints"));
        spCol.setMinWidth(50);
        spCol.setResizable(true);
        foodTypeCol.setCellValueFactory(new PropertyValueFactory<>("sourceType"));
        foodTypeCol.setMinWidth(100);
        foodTypeCol.setResizable(true);
        portionCol.setCellValueFactory(new PropertyValueFactory("servingDesc"));
        portionCol.setResizable(true);
        pAmountCol.setCellValueFactory(new PropertyValueFactory("portionSize"));
        pAmountCol.setResizable(true);
        pUnitCol.setCellValueFactory(new PropertyValueFactory("portionName"));
        pUnitCol.setResizable(true);
        
        newTable.getColumns().addAll(nameCol, spCol, foodTypeCol, portionCol, pAmountCol, pUnitCol);
        newTable.setMinSize(850, 700);
        
        return newTable;
    }    
}
