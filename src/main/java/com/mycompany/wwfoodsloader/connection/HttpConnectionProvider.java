/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.connection;

import com.google.gson.Gson;
import com.mycompany.wwfoodsloader.beans.FoodBean;
import com.mycompany.wwfoodsloader.beans.QueryBean;
import com.mycompany.wwfoodsloader.beans.QueryHitsBean;
import com.mycompany.wwfoodsloader.beans.QueryResponse;
import com.mycompany.wwfoodsloader.beans.ResponseBean;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleDoubleProperty;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author simon
 */
public class HttpConnectionProvider {
    private final static HttpConnectionProvider instance = new HttpConnectionProvider();
    
    private final static String USERNAME = "ASMUSJULIA";
    private final static String PASSWORD = "Geldbeutel2";
    
    private final static String LOGIN_HOST = "https://login.weightwatchers.de";
    private final static String CMX_HOST = "https://cmx.weightwatchers.de";
    
    private final static String RECENT_FOODS_URL = "/api/v2/cmx/operations/composed/members/~/lists/recent?isActive=true&fullDetails=true";
    private final static String FAVORITE_FOODS_URL = "/api/v2/cmx/operations/composed/members/~/lists/favorite";
    private final static String QUERY_PERSONAL_FOODS_URL = "/api/v2/search/personalfoods";
    private final static String QUERY_FOODS_URL = "/api/v2/search/unified";
    
    private final static String QUERY_PERSONAL_FOODS_PARAMS_TEMPLATE = "?query=%s&attributesToRetrieve=isPowerFood,sourceId,_ingredientName,portionName,versionId,smartPoints,_servingDesc,sourceType,_portionDisplayName,points,isFavorite,name,portionId,_id,qty,type,_displayName&facetFilters=[[\"sourceType:WWFOOD\",\"sourceType:WWVENDORFOOD\",\"sourceType:MEMBERFOOD\",\"sourceType:MEMBERMEAL\",\"sourceType:WWMEAL\",\"sourceType:MEMBERRECIPE\",\"sourceType:WWRECIPE\"],\"isFavorite:true\"]&page=0&cmxConfig=4&analyticsTags=M,Personal";
    private final static String QUERY_FOODS_PARAMS_TEMPLATE = "?query=%s&attributesToRetrieve=isPowerFood,sourceId,_ingredientName,portionName,versionId,smartPoints,_servingDesc,_portionDisplayName,sourceType,code,points,isFavorite,name,portionId,_id,qty,_displayName,type&cmxConfig=4&analyticsTags=M,Personal";
    
    private final Map<String, String> requestPropertiesMap = new HashMap<>();
    // Create a trust manager that does not validate certificate chains
    private final TrustManager[] trustAllCerts = new TrustManager[] { 
        new X509TrustManager() {     
            @Override
            public X509Certificate[] getAcceptedIssuers() { 
                return new X509Certificate[0];
            } 
            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                // EMPTY
            } 
            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                // EMPTY
            }
        } 
    }; 
    
    private HttpsURLConnection httpsConnection;
    private SimpleDoubleProperty progress = new SimpleDoubleProperty(0D);
    
    private HttpConnectionProvider() {
        CookieHandler.setDefault(new CookieManager());
        
        final StringBuilder builder = new StringBuilder();
        builder.append("amlbcookie=13");
        builder.append(";");
        builder.append(".ASPXAUTH=D4A8F2F76A372CC6D1781039545552A48BF676D8DBB1E0FAF1C58B0A4A516C90489FD6657054B82389A4F2B0B48CB8EE92CB49B87C95A88DD6CC7FC271096006074B0A63D87C5C2EB407BB58E56F3EE358E58CF1A4C4EAC9D6FF9FEBA981C935F32045EBA2A2EB29");
        builder.append(";");
        builder.append("AUTOLOGIN=\"ALNAME=4tdBM1Mt9B9MGYIDRvAAmw==&ALPWD=4+yd+xdln1AxEfGZJx9+Nk6ccHseVWjT&IsEnabled=True\"");
        builder.append(";");
        builder.append("$Path=\"/\"");
        builder.append(";");
        builder.append("$Domain=\".weightwatchers.de\"");
        builder.append(";");
        builder.append("LoginStatus=D44AC90B1E8C0EE8C99EAE770A1998D60B6AC2808C94DD2637A698D525756D120ABD11410B5BA1F45B082C6DDCF2DCCD768F4B5ED08D856A7DB22AC06AACA742");
        builder.append(";");
        builder.append("TS0150f825=01ae181aad06a66057afbc1e2e81d5c2cac245169efb0a86ded6eecfca25bd9ea2070741cf630a7699f966625c9e221086873fcdad62f4c2a3b96915c3ba6cf6f7c2e213f3f81486f4c7453929907edfaa7c256d68140563dafaf244cdd6faf143ef3f7c71d9b52b7007fbf986817842a527a63b61");
        builder.append(";");
        builder.append("TS01f04cd4=01ae181aad060485160d81bbea886100f1bf6cdf90a6c19e9056420289abaec283ac3e7964");
        builder.append(";");
        builder.append("WWTracking=MN=AF751345240DC96D87346752861DA9A7");
        builder.append(";");
        builder.append("WeightWatchersCorp=WORKID=");
        builder.append(";");
        builder.append("iPlanetDirectoryPro=AQIC5wM2LY4SfczHHi5vz0dqDEYYDm5VsRRyF3_FYbju0Xk.*AAJTSQACMDMAAlNLABQtNjkzNTE3Mzk3NDA2ODIwODg4NAACUzEAAjEz");
        builder.append(";");
        builder.append("profile=s:j:{\"uuid\":\"c884af5e-41c0-4cea-9155-a2104def3675\"}.gxiQ/eupUjEecvskYUhRIK8Jwgj0LxRzhcC/h9fDVbM");
        builder.append(";");
        builder.append("shouldGetProfileFromSoa=false");
        requestPropertiesMap.put("Cookie", builder.toString());
        
        requestPropertiesMap.put("X-NewRelic-ID", "XAIHU1dWGwIBVFlVAQcB");
        requestPropertiesMap.put("Host", "cmx.weightwatchers.de");
        requestPropertiesMap.put("Connection", "close");
        requestPropertiesMap.put("Accept-Encoding", "gzip");
        requestPropertiesMap.put("User-Agent", "okhttp/2.7.5");
    }

    public static HttpConnectionProvider getInstance() {
        return instance;
    }
    
    private String openConnection() {
        return "";
    }
    
    public List<FoodBean> getRecentFoods() {
        try {
            final URL recentFoodsUrl = new URL(CMX_HOST + RECENT_FOODS_URL);
            return(sendPost(requestPropertiesMap, recentFoodsUrl));
        }
        catch (MalformedURLException mue) {
            mue.printStackTrace();
        }
        return null;
    }
    
    public List<FoodBean> sendPost(final Map<String, String> requestPropsMap, final URL url) {
        List<FoodBean> retVal = null;
        progress.set(0D);
        
        final StringBuilder content = new StringBuilder();
        try {
            final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 8080));
            final SSLContext sc = SSLContext.getInstance("SSL"); 
            sc.init(null, trustAllCerts, new SecureRandom()); 
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            progress.set(0.1D);
            httpsConnection = (HttpsURLConnection) url.openConnection(proxy);
            httpsConnection.setUseCaches(false);
            httpsConnection.setRequestMethod("GET");
            httpsConnection.setDoInput(true);
            httpsConnection.addRequestProperty("Accept", "");
            progress.set(0.2D);
            for (Map.Entry<String, String> mapEntry : requestPropsMap.entrySet()) {
                httpsConnection.addRequestProperty(mapEntry.getKey(), mapEntry.getValue());
            }
            progress.set(0.3D);
            try (InputStreamReader in = new InputStreamReader((InputStream) httpsConnection.getContent(), "UTF-8");
                    BufferedReader buffer = new BufferedReader(in);) {
                String tmp = null;
                progress.set(0.4D);
                while ((tmp = buffer.readLine()) != null) {
                    content.append(tmp);
                }
                progress.set(0.5D);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            
            if (content != null) {
                progress.set(0.6D);
                final Gson gson = new Gson();
                final ResponseBean bean = gson.fromJson(content.toString(), ResponseBean.class);
                retVal = bean.getHits();
                progress.set(0.7D);
            }
        }
        catch (MalformedURLException mup) {
            mup.printStackTrace();
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (KeyManagementException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()); 
        try (FileOutputStream fileOut = new FileOutputStream("/home/simon/Downloads/2_ww/response." + timeStamp);
                OutputStreamWriter out = new OutputStreamWriter(fileOut)) {
            progress.set(0.8D);
            out.write(content.toString());
            out.flush();
        }
        catch (IOException ioe) {
                ioe.printStackTrace();
        }
        progress.set(1D);
        return retVal;
    }
    
    public List<FoodBean> getFavoriteFoods() {
        try {
            final URL recentFoodsUrl = new URL(CMX_HOST + FAVORITE_FOODS_URL);
            return(sendPost(requestPropertiesMap, recentFoodsUrl));
        }
        catch (MalformedURLException mue) {
            mue.printStackTrace();
        }
        return null;
    }
    
    public List<QueryHitsBean> queryFoods(final String searchString) {
        try {
            final URL searchUrl = new URL(CMX_HOST + QUERY_FOODS_URL + String.format(QUERY_FOODS_PARAMS_TEMPLATE, searchString));
            return(sendQueryPost(requestPropertiesMap, searchUrl));
        } 
        catch (MalformedURLException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private List<QueryHitsBean> sendQueryPost(final Map<String, String> requestPropsMap, final URL searchUrl) {
        final List<QueryHitsBean> retVal = new ArrayList<>();
        progress.set(0);
        final StringBuilder content = new StringBuilder();
        try {
            final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 8080));
            final SSLContext sc = SSLContext.getInstance("SSL"); 
            sc.init(null, trustAllCerts, new SecureRandom()); 
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            progress.setValue(10/100);
            httpsConnection = (HttpsURLConnection) searchUrl.openConnection(proxy);
            httpsConnection.setUseCaches(false);
            httpsConnection.setRequestMethod("GET");
            httpsConnection.setDoInput(true);
            httpsConnection.addRequestProperty("Accept", "");
                    
            for (Map.Entry<String, String> mapEntry : requestPropsMap.entrySet()) {
                httpsConnection.addRequestProperty(mapEntry.getKey(), mapEntry.getValue());
            }
            progress.setValue(15/100);
            try (InputStreamReader in = new InputStreamReader((InputStream) httpsConnection.getContent(), "UTF-8");
                    BufferedReader buffer = new BufferedReader(in);) {
                String tmp = null;
                while ((tmp = buffer.readLine()) != null) {
                    content.append(tmp);
                    progress.setValue(progress.get() + 5/100);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            
            if (content.length() > 0) {
                progress.setValue(progress.get() + 10/100);
                final Gson gson = new Gson();
                final QueryBean bean = gson.fromJson(content.toString(), QueryBean.class);
                retVal.addAll(bean.getTopHits() != null ? bean.getTopHits().getHits() : null);
                retVal.addAll(bean.getFood() != null ? bean.getFood().getHits() : null);
                progress.setValue(progress.get() + 5/100);
            }
        }
        catch (MalformedURLException mup) {
            mup.printStackTrace();
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (KeyManagementException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()); 
        progress.setValue(progress.get() + 10/100);
        try (FileOutputStream fileOut = new FileOutputStream("/home/simon/Downloads/2_ww/response." + timeStamp);
                OutputStreamWriter out = new OutputStreamWriter(fileOut)) {
            progress.setValue(progress.get() + 10/100);
            out.write(content.toString());
            out.flush();
        }
        catch (IOException ioe) {
                ioe.printStackTrace();
        }
        progress.setValue(1.0);
        return retVal;
    }
    
    public List<QueryHitsBean> queryFavoriteFoods(String searchString) {
        try {
            final URL searchUrl = new URL(CMX_HOST + QUERY_PERSONAL_FOODS_URL + String.format(QUERY_PERSONAL_FOODS_PARAMS_TEMPLATE, searchString));
            return(sendFavQueryPost(requestPropertiesMap, searchUrl));
        } 
        catch (MalformedURLException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private List<QueryHitsBean> sendFavQueryPost(final Map<String, String> requestPropsMap, final URL searchUrl) {
        final List<QueryHitsBean> retVal = new ArrayList<>();
        progress.set(0);
        final StringBuilder content = new StringBuilder();
        try {
            final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 8080));
            final SSLContext sc = SSLContext.getInstance("SSL"); 
            sc.init(null, trustAllCerts, new SecureRandom()); 
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            progress.setValue(10/100);
            httpsConnection = (HttpsURLConnection) searchUrl.openConnection(proxy);
            httpsConnection.setUseCaches(false);
            httpsConnection.setRequestMethod("GET");
            httpsConnection.setDoInput(true);
            httpsConnection.addRequestProperty("Accept", "");
                    
            for (Map.Entry<String, String> mapEntry : requestPropsMap.entrySet()) {
                httpsConnection.addRequestProperty(mapEntry.getKey(), mapEntry.getValue());
            }
            progress.setValue(15/100);
            try (InputStreamReader in = new InputStreamReader((InputStream) httpsConnection.getContent(), "UTF-8");
                    BufferedReader buffer = new BufferedReader(in);) {
                String tmp = null;
                while ((tmp = buffer.readLine()) != null) {
                    content.append(tmp);
                    progress.setValue(progress.get() + 5/100);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            
            if (content.length() > 0) {
                progress.setValue(progress.get() + 10/100);
                final Gson gson = new Gson();
                final QueryResponse bean = gson.fromJson(content.toString(), QueryResponse.class);
                retVal.addAll(bean.getHits() != null ? bean.getHits() : null);
                progress.setValue(progress.get() + 5/100);
            }
        }
        catch (MalformedURLException mup) {
            mup.printStackTrace();
        } 
        catch (IOException ex) {
            ex.printStackTrace();
        } 
        catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (KeyManagementException ex) {
            Logger.getLogger(HttpConnectionProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()); 
        progress.setValue(progress.get() + 10/100);
        try (FileOutputStream fileOut = new FileOutputStream("/home/simon/Downloads/2_ww/response." + timeStamp);
                OutputStreamWriter out = new OutputStreamWriter(fileOut)) {
            progress.setValue(progress.get() + 10/100);
            out.write(content.toString());
            out.flush();
        }
        catch (IOException ioe) {
                ioe.printStackTrace();
        }
        progress.setValue(1.0);
        return retVal;
    }
    
    public double getProgress() {
        return progress.doubleValue();
    }
    
    public SimpleDoubleProperty progressProperty() {
        return progress;
    }

}
