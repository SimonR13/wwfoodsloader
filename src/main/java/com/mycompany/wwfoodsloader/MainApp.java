package com.mycompany.wwfoodsloader;

import java.io.IOException;
import java.io.InputStream;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("WeightWatchers SmartPoints Saver");

        stage.setScene(
            createScene(
                loadMainPane()
            )
        );

        stage.show();
    }

    private Pane loadMainPane() throws IOException {
        final FXMLLoader loader = new FXMLLoader();
        Pane mainPane = null;
        
        try (InputStream in = getClass().getResourceAsStream(VistaNavigator.MAIN);) {
            mainPane = (Pane) loader.load(in);
        }
        catch (IOException ioe) {
            System.err.println("Exception: " + ioe.getMessage());
        }
        
        MainController mainController = loader.getController();

        VistaNavigator.setMainController(mainController);
        VistaNavigator.loadVista(VistaNavigator.VISTA_1);

        return mainPane;
    }
    
    private Scene createScene(Pane mainPane) {
        Scene scene = new Scene(
            mainPane
        );

        scene.getStylesheets().setAll(
            getClass().getResource("/styles/Styles.css").toExternalForm()
        );

        return scene;
    }
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
