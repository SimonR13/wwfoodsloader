/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.mycompany.wwfoodsloader.beans.FoodBean;
import com.mycompany.wwfoodsloader.beans.QueryBean;
import com.mycompany.wwfoodsloader.beans.QueryHitsBean;
import com.mycompany.wwfoodsloader.beans.ResponseBean;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class ResponseParser {
    private final static ResponseParser instance = new ResponseParser();
    
    private ResponseParser() {
        // EMPTY
    }
    
    public static ResponseParser getInstance() {
        return instance;
    }
    
    public File openFile(String filePath) {
        File retVal = null;
        if (filePath != null && filePath.trim().length() > 0) {
            try {
                retVal = new File(filePath.trim());
            }
            catch (NullPointerException npe) {
                System.err.println(npe.getMessage());
                npe.printStackTrace();
            }
        }
        return retVal;
    }
    
    public List<FoodBean> parseFileForWwFoodsInComposed(final File responseFile) {
        final List<FoodBean> retVal = new ArrayList<>();
        if (responseFile != null && responseFile.isFile() && responseFile.canRead()) {
            try (JsonReader reader = new JsonReader(new FileReader(responseFile))) {
                final Gson gSon = new Gson();
                
                final ResponseBean wwFood = gSon.fromJson(reader, ResponseBean.class);
                if (wwFood != null && wwFood.getHits() != null) {
                    retVal.addAll(wwFood.getHits());
                }
            } 
            catch (IllegalStateException | IOException ise) {
                Logger.getLogger(ResponseParser.class.getName()).log(Level.SEVERE, null, ise);
            }
        }
        return retVal;
    }
    
    public List<QueryHitsBean> parseFileForFoodInQuery(final File responseFile) {
        final List<QueryHitsBean> retVal = new ArrayList<>();
        if (responseFile != null && responseFile.isFile() && responseFile.canRead()) {
            try (JsonReader reader = new JsonReader(new FileReader(responseFile))) {
                final Gson gSon = new Gson();
                
                final QueryBean reBean = gSon.fromJson(reader, QueryBean.class);
                if (reBean != null && reBean.getTopHits() != null) {
                    retVal.addAll(reBean.getTopHits().getHits());
                    if (reBean.getFood() != null && reBean.getFood().getHits() != null) {
                        retVal.addAll(reBean.getFood().getHits());
                    }
                }
            }
            catch (IllegalStateException | IOException ex) {
                Logger.getLogger(ResponseParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retVal;
    }
    
    public boolean writeFoodBeansToFile(final File outFile, final List<FoodBean> beanList) {
        boolean retVal = false;
        
        if (outFile != null && outFile.isFile() && outFile.canWrite()) {
            try (JsonWriter writer = new JsonWriter(new FileWriter(outFile))) {
                Gson gson = new Gson();
                gson.toJson(beanList, FoodBean.class, writer);
                writer.flush();
            } 
            catch (IOException ex) {
                Logger.getLogger(ResponseParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        return retVal;
    }
    
}
