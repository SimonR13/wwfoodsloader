/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author simon
 */
public class QueryResponse {
    
    @SerializedName("hits")
    private List<QueryHitsBean> hits;

    public List<QueryHitsBean> getHits() {
        return hits;
    }

    public void setHits(List<QueryHitsBean> hits) {
        this.hits = hits;
    }
    
}
