/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author simon
 */
public class QueryBean {
    
    @SerializedName("topHits")
    private QueryResponse topHits;
    @SerializedName("food")
    private QueryResponse food;

    public QueryResponse getTopHits() {
        return topHits;
    }

    public void setTopHits(QueryResponse topHits) {
        this.topHits = topHits;
    }

    public QueryResponse getFood() {
        return food;
    }

    public void setFood(QueryResponse food) {
        this.food = food;
    }

}
