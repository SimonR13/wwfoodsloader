/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author simon
 */
public class QueryHitsBean {
    
    @SerializedName("_id")
    private String id;
    @SerializedName("_displayName")
    private String displayName;
    @SerializedName("_servingDesc")
    private String servingDesc;
    @SerializedName("_ingredientName")
    private String ingredientName;
    
    private String versionId;
    private String portionId;
    private String sourceType;
    private String type;
    private String smartPoints;
    private String points;
    private String qty;
    private String isPowerFood;
    private String objectID;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getServingDesc() {
        return servingDesc;
    }

    public void setServingDesc(String servingDesc) {
        this.servingDesc = servingDesc;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getPortionId() {
        return portionId;
    }

    public void setPortionId(String portionId) {
        this.portionId = portionId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSmartPoints() {
        return smartPoints;
    }

    public void setSmartPoints(String smartPoints) {
        this.smartPoints = smartPoints;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIsPowerFood() {
        return isPowerFood;
    }

    public void setIsPowerFood(String isPowerFood) {
        this.isPowerFood = isPowerFood;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }
}
