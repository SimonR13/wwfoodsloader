/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import java.util.Map;

/**
 *
 * @author simon
 */
public class TrackedCountsBean {
    private String _id;

    private Map<String, String[]> month;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public Map<String, String[]> getMonth() {
        return month;
    }

    public void setMonth(Map<String, String[]> month) {
        this.month = month;
    }
    
}
