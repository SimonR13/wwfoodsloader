/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author simon
 */
public class FoodBean {
    
    private String entryId;
    private String _id;
    private String foodEntityId;
    private String name;
    private String sourceType;
    private String foodType;
    private String sourceId;
    private String versionId;
    private String _displayName;
    private String _ingredientName;
    private String portionId;
    private String portionName;
    private String sourcePortionId;
    private String portionSize;
    private String quantity;
    @SerializedName("_servingDesc")
    private String servingDesc;
    private String points;
    private String pointsPrecise;
    private String smartPoints;
    private String smartPointsPrecise;
    private String isPowerFood;
    private String isActive;
    private String _portionDisplayName;
    private String type;
    private String qty;
    private String objectID;
    
    private TrackedCountsBean trackedCounts;
    private List<PortionsBean> portions;
    private FavoriteEntryBean favoriteEntry;

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getFoodEntityId() {
        return foodEntityId;
    }

    public void setFoodEntityId(String foodEntityId) {
        this.foodEntityId = foodEntityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public void setDisplayName(String _displayName) {
        this._displayName = _displayName;
    }

    public String getIngredientName() {
        return _ingredientName;
    }

    public void setIngredientName(String _ingredientName) {
        this._ingredientName = _ingredientName;
    }

    public String getPortionId() {
        return portionId;
    }

    public void setPortionId(String portionId) {
        this.portionId = portionId;
    }

    public String getPortionName() {
        return portionName;
    }

    public void setPortionName(String portionName) {
        this.portionName = portionName;
    }

    public String getSourcePortionId() {
        return sourcePortionId;
    }

    public void setSourcePortionId(String sourcePortionId) {
        this.sourcePortionId = sourcePortionId;
    }

    public String getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(String portionSize) {
        this.portionSize = portionSize;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getServingDesc() {
        return servingDesc;
    }

    public void setServingDesc(String _servingDesc) {
        this.servingDesc = _servingDesc;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPointsPrecise() {
        return pointsPrecise;
    }

    public void setPointsPrecise(String pointsPrecise) {
        this.pointsPrecise = pointsPrecise;
    }

    public String getSmartPoints() {
        return smartPoints;
    }

    public void setSmartPoints(String smartPoints) {
        this.smartPoints = smartPoints;
    }

    public String getSmartPointsPrecise() {
        return smartPointsPrecise;
    }

    public void setSmartPointsPrecise(String smartPointsPrecise) {
        this.smartPointsPrecise = smartPointsPrecise;
    }

    public String getIsPowerFood() {
        return isPowerFood;
    }

    public void setIsPowerFood(String isPowerFood) {
        this.isPowerFood = isPowerFood;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPortionDisplayName() {
        return _portionDisplayName;
    }

    public void setPortionDisplayName(String _portionDisplayName) {
        this._portionDisplayName = _portionDisplayName;
    }

    public List<PortionsBean> getPortions() {
        return portions;
    }

    public void setPortions(List<PortionsBean> portions) {
        this.portions = portions;
    }

    public TrackedCountsBean getTrackedCounts() {
        return trackedCounts;
    }

    public void setTrackedCounts(TrackedCountsBean trackedCounts) {
        this.trackedCounts = trackedCounts;
    }

    public FavoriteEntryBean getFavoriteEntry() {
        return favoriteEntry;
    }

    public void setFavoriteEntry(FavoriteEntryBean favoriteEntry) {
        this.favoriteEntry = favoriteEntry;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }
    
}
