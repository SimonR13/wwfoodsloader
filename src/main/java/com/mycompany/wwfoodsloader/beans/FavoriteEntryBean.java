/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

/**
 *
 * @author simon
 */
public class FavoriteEntryBean {
    private String portionId;
    private String versionId;
    private String quantity;
    private String isFavorite;
    private String favoriteEntryId;

    public String getPortionId() {
        return portionId;
    }

    public void setPortionId(String portionId) {
        this.portionId = portionId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getFavoriteEntryId() {
        return favoriteEntryId;
    }

    public void setFavoriteEntryId(String favoriteEntryId) {
        this.favoriteEntryId = favoriteEntryId;
    }
    
}
