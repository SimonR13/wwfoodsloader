/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author simon
 */
public class PortionsBean {
    @SerializedName("default")
    private String defaultStr;
    private String name;
    private String size;
    private String weight;
    private String portionTypeId;
    private String weightType;
    private String sourceId;
    private String _id;
    private String smartPointsPrecise;
    private String pointsPrecise;
    private String isActive;
    private String points;
    private String smartPoints;
    private String _displayName;
    private String _servingDesc;
    
    private NutrionBean nutrition;

    public String getDefaultStr() {
        return defaultStr;
    }

    public void setDefaultStr(String defaultStr) {
        this.defaultStr = defaultStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPortionTypeId() {
        return portionTypeId;
    }

    public void setPortionTypeId(String portionTypeId) {
        this.portionTypeId = portionTypeId;
    }

    public String getWeightType() {
        return weightType;
    }

    public void setWeightType(String weightType) {
        this.weightType = weightType;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public NutrionBean getNutrition() {
        return nutrition;
    }

    public void setNutrition(NutrionBean nutrition) {
        this.nutrition = nutrition;
    }

    public String getSmartPointsPrecise() {
        return smartPointsPrecise;
    }

    public void setSmartPointsPrecise(String smartPointsPrecise) {
        this.smartPointsPrecise = smartPointsPrecise;
    }

    public String getPointsPrecise() {
        return pointsPrecise;
    }

    public void setPointsPrecise(String pointsPrecise) {
        this.pointsPrecise = pointsPrecise;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getSmartPoints() {
        return smartPoints;
    }

    public void setSmartPoints(String smartPoints) {
        this.smartPoints = smartPoints;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public void setDisplayName(String _displayName) {
        this._displayName = _displayName;
    }

    public String getServingDesc() {
        return _servingDesc;
    }

    public void setServingDesc(String _servingDesc) {
        this._servingDesc = _servingDesc;
    }
    
}
