/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author simon
 */
public class ResponseBean {
    
    @SerializedName("hits")
    private List<FoodBean> hits;

    public List<FoodBean> getHits() {
        return hits;
    }

    public void setHits(List<FoodBean> hits) {
        this.hits = hits;
    }
    
}
