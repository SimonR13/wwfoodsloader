/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wwfoodsloader.beans;

/**
 *
 * @author simon
 */
public class FoodBeanFactory {
    public static FoodBean getFoodBeanFromQueryHit(QueryHitsBean queryHitsBean) {
        final FoodBean retVal = new FoodBean();
        retVal.setId(queryHitsBean.getId());
        retVal.setDisplayName(queryHitsBean.getDisplayName());
        retVal.setServingDesc(queryHitsBean.getServingDesc());
        retVal.setIngredientName(queryHitsBean.getIngredientName());
        retVal.setVersionId(queryHitsBean.getVersionId());
        retVal.setPortionId(queryHitsBean.getPortionId());
        retVal.setSourceType(queryHitsBean.getSourceType());
        retVal.setType(queryHitsBean.getType());
        retVal.setSmartPoints(queryHitsBean.getSmartPoints());
        retVal.setPoints(queryHitsBean.getPoints());
        retVal.setQuantity(queryHitsBean.getQty());
        retVal.setIsPowerFood(queryHitsBean.getIsPowerFood());
        retVal.setObjectID(queryHitsBean.getObjectID());
        return retVal;
    }
}
